Questa portlet può essere utilizzata come pagina di benvenuto per il portale e può dare l'accesso alla navigazione libera oppure alla pagina di Login.

Per il suo utilizzo è necessario creare una pagina iniziale ad una colonna ed inserirvi come unica portlet "Essential Overlay".

Si raccomanda inoltre di chiamare la pagina che di default su Liferay è Welcome come Home/home.