<%@page import="java.net.URLEncoder"%>
<%@page import="javax.portlet.PortletSession"%>
<%@page import="com.welive.controller.util.Constants"%>
<%@page import="com.welive.controller.portlet.ControllerPortlet"%>
<%@taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
	int nView = 1;
	String contextPath = request.getContextPath();
	String imgPath = contextPath + "/img";
%>

<portlet:actionURL var="selectToolURL" windowState="normal" name="selectTool">
</portlet:actionURL>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
boolean logged = false;
String colBottomRow = "s12 m12 l12";
String pilot = (String)portletSession.getAttribute("LIFERAY_SHARED_pilot", PortletSession.APPLICATION_SCOPE);
try {
	if(themeDisplay.isSignedIn() && user != null) {
		logged = true;
		// Bottom tool row: Column layout
		colBottomRow = "s12 m12 l6";
		if(pilot == null) {
			String[] pilotArray = (String [])user.getExpandoBridge().getAttribute("pilot");
			if(pilotArray != null && pilotArray.length > 0) {
				pilot = pilotArray[0];
				portletSession.setAttribute("LIFERAY_SHARED_pilot", pilot);
			}
		}
	}
} catch(Exception e) {}
%>

		<div id="materialize-body" class="materialize">
			<div id="pilot" class="section">
<%if(pilot == null) {%>	
				<div id="pilot-unknown" class="container row center">
					<div class="col s12 m12 l12 center margin-vertical">
						<img src="<%=imgPath%>/logo_welive.png">
					</div>
					<div class="col s12 m12 l12 margin-vertical"></div>
					<h5 class="col s12 m12 l12 grey-text text-darken-4 margin-vertical"><liferay-ui:message key="overlay.header" /></h5>
					<div class="col s12 m12 l12 margin-vertical"></div>
					<h5 class="col s12 m12 l12 grey-text text-darken-4 margin-vertical"><liferay-ui:message key="overlay.tools.header" /></h5>
				</div>
<%} else {%>
				<div class="container">
					<div class="card">
	            		<div class="card-image" style="text-align: center;">
	<%if(Constants.PILOT_CITY_BILBAO.equals(pilot)) {%>
							<img src="<%=imgPath%>/bilbao_about.jpg"/>
	<%} else if(Constants.PILOT_CITY_HELSINKI.equals(pilot)) {%>
							<img src="<%=imgPath%>/helsinki_about.jpg"/>
	<%} else if(Constants.PILOT_CITY_NOVISAD.equals(pilot)) {%>
							<img src="<%=imgPath%>/novi_sad_about.png"/>
	<%} else if(Constants.PILOT_CITY_TRENTO.equals(pilot)) {%>
							<img src="<%=imgPath%>/trento_about.jpg"/>
	<%}%>
							<span class="card-title grey-text text-lighten-2">
								<strong><liferay-ui:message key="overlay.tools.header" /></strong>
							</span>
						</div>
					</div>
				</div>
<%}%>
			</div>
				
				
			<div id="selector" class="section">
				<div class="container row">
						<div class="icon-block col s12 m12 l6">
							<h2 class="center lime-text">
								<a href="<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_OIA, "UTF-8")%>">
									<i class="fa fa-lightbulb-o"></i>
								</a>
							</h2>
							<h5 class="center"><liferay-ui:message key="overlay.tools.oia" /></h5>
							<p class="light"><liferay-ui:message key="overlay.tools.oia.description" /></p>
						</div>
						
						<div class="icon-block col s12 m12 l6">
							<h2 class="center lime-text">
								<a href="<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_MKP, "UTF-8")%>">
									<i class="fa fa-shopping-cart"></i>
								</a>
							</h2>
							<h5 class="center"><liferay-ui:message key="overlay.tools.marketplace" /></h5>
							<p class="light"><liferay-ui:message key="overlay.tools.marketplace.description" /></p>
						</div>
				</div>		
				<div class="container row">
						<div class="icon-block col <%= colBottomRow %>">
							<h2 class="center lime-text">
								<a href="<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_ODS, "UTF-8")%>">
									<i class="fa fa-database"></i>
								</a>
							</h2>
							<h5 class="center"><liferay-ui:message key="overlay.tools.ods" /></h5>
							<p class="light"><liferay-ui:message key="overlay.tools.ods.description" /></p>
						</div>
<% 
if(logged) {
%>
						<div class="icon-block col <%= colBottomRow %>">
							<h2 class="center lime-text">
								<a href="<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_ADS, "UTF-8")%>">
									<i class="fa fa-pie-chart"></i>
								</a>
							</h2>
							<h5 class="center"><liferay-ui:message key="overlay.tools.dashboard" /></h5>
							<p class="light"><liferay-ui:message key="overlay.tools.dashboard.description" /></p>
						</div>
<% 
}
%>
				</div>
<%-- 	
				<div class="container row">
						<div class="icon-block col s12 m12 l6">
							<h2 class="center lime-text">
								<a href="<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_CDV, "UTF-8")%>">
									<i class="fa fa-user-secret"></i>
								</a>
							</h2>
							<h5 class="center"><liferay-ui:message key="overlay.tools.cdv" /></h5>
							<p class="light"><liferay-ui:message key="overlay.tools.cdv.description" /></p>
						</div>
						<div class="icon-block col s12 m12 l6">
							<h2 class="center lime-text">
								<a href="<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_VC, "UTF-8")%>">
									<i class="material-icons">developer_board</i>
								</a>
							</h2>
							<h5 class="center"><liferay-ui:message key="overlay.tools.vc" /></h5>
							<p class="light"><liferay-ui:message key="overlay.tools.vc.description" /></p>
						</div>
				</div>
--%>
			</div>
			
		</div>
		
