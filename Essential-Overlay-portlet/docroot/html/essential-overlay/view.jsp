<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.welive.controller.util.Constants"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.json.JSONArray"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil"%>
<%@page import="it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil"%>
<%@page import="javax.portlet.PortletSession"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Locale"%>
<%@page import="com.welive.controller.util.PropertyGetter"%>
<%@page import="com.liferay.portal.kernel.util.PrefsPropsUtil"%>
<%@page import="java.util.ResourceBundle" %>
<%@page import="com.liferay.portal.model.Role"%>
<%@taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
Log _log = LogFactoryUtil.getLog("Controller");
String casLoginUrl = "/c/portal/login";
String pilotr="";
boolean login = false;
HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(request);
pilotr = httpRequest.getParameter("pilot");

if("true".equals(httpRequest.getParameter("login"))) {
	if(user == null || !themeDisplay.isSignedIn()) {
		login = true;
	}
}

%>
<c:choose>
    
    <c:when test="<%=login%>">
        <script>
            location.replace("<%=casLoginUrl%>");
        </script>
    </c:when>
    <c:otherwise>

    </c:otherwise>

</c:choose>




<portlet:defineObjects />
<theme:defineObjects />




<%
	String role = "ROLE-";
	JSONArray ideas = null;
	JSONArray challenges = null;
	
	JSONArray needs = null;
	JSONArray artifacts = null;
	String pilot="";
	boolean test = false;
	String pilotforchart = "['Bilbao','Bilbao', 'Bilbao', 'Bilbao']";
	
	boolean isLoged =false;
	
	boolean isAuthority = false;
	boolean isDeveloper = false;
	boolean isCompanyLeader = false;
	boolean isCitizen  = false;
	boolean isAcademy = false;
	boolean isBussines = false;	
	boolean isSimpleUser = false;
	boolean isEntrepreneur = false;	
	
	boolean userHasIdeas = false;
	boolean userHasNeeds = false;
	boolean userHasArtefacts = false;
	boolean userHasChallenges = false;
	
	boolean pilotHasIdeas = false;
	boolean pilotHasNeeds = false;
	boolean pilotHasArtefacts = false;
	boolean pilotHasChallenges = false;
	
	boolean langHasIdeas = false;
	boolean langHasNeeds = false;
	boolean langHasArtefacts = false;
	boolean langHasChallenges = false;
	
	PropertyGetter propertyGetter = PropertyGetter.getInstance();
	String baseurl ="";
	Long ccUserId = 0L;
	String languaje ="";
	Locale loc = null;
	boolean debug = false;
	try{		
		baseurl = propertyGetter.getProperty("welive.api.url");
		String sdebug = propertyGetter.getProperty("debug");
		if (sdebug.equals("true")){
			debug = true;
		}
		if (baseurl.contains("test")) test = true;
		
		//(String)renderRequest.getAttribute("baseurl");//ParamUtil.getString(request,baseurl);
		loc = themeDisplay.getLocale();
		languaje = loc.getLanguage();	
		if (themeDisplay.isSignedIn() && user != null){
			isLoged = true;
			_log.info("isLoged: true");
			ccUserId = (Long)user.getExpandoBridge().getAttribute("CCUserID");
			/*if (ccUserId==0) {
				ccUserId = 303L;
				isAuthority = true;
				role= "Authority";
			}*/
	        List<Role> roleList = user.getRoles();
	        for(Role rol : roleList) {
	        	//Authority/Municipality/companyLeader citizen/academy/business
	        	role+="-"+rol.getName();
	        	
	        	if (rol.getName().equalsIgnoreCase(Constants.ROLE_AUTHORITY) || rol.getName().equalsIgnoreCase(Constants.ROLE_ADMIN)){
	        		isAuthority = true;
	        	}
	        	if (rol.getName().equalsIgnoreCase(Constants.ROLE_DEVELOPER)){
	        		isDeveloper = true;
	        	}
	        	if (rol.getName().equalsIgnoreCase(Constants.ROLE_COMPANY_LEADER)){
	        		isCompanyLeader = true;
	        	}
	        	if (rol.getName().equalsIgnoreCase(Constants.ROLE_CITIZEN)){
	        		isCitizen = true;
	        	}
	        	if (rol.getName().equalsIgnoreCase(Constants.ROLE_ACADEMY)){
	        		isAcademy = true;
	        	}
	        	if (rol.getName().equalsIgnoreCase(Constants.ROLE_BUSINESS)){
	        		isBussines = true;    		
	        	}
	        	if (rol.getName().equalsIgnoreCase(Constants.ROLE_ENTREPRENEURE)){
	        		isEntrepreneur = true;    		
	        	}
	        		
	        }
			if (isAcademy || isBussines || isCitizen || isEntrepreneur){
				isSimpleUser = true;
			}
				        
			String[] pilotArray = (String [])user.getExpandoBridge().getAttribute("pilot");
		     if(pilotArray != null && pilotArray.length > 0) {
		        pilot = pilotArray[0];
		        _log.info("pilot: "+pilot);
		        //if (pilot==null || pilot.equals("")) pilot ="Trento";
		    }
		    if (!debug){
			    try{
		        	_log.info("get Ideas by user");
					ideas = CLSIdeaLocalServiceUtil.getIdeasOrNeedsByUserId(user.getUserId(), false, 3,loc);
		        	_log.info("get Needs by user");
					needs = CLSIdeaLocalServiceUtil.getIdeasOrNeedsByUserId(user.getUserId(), true, 3,loc);
		        	_log.info("get Artefacts by user");
					artifacts = ArtefattoLocalServiceUtil.getArtefactsByUserId(user.getUserId(),3);
		        	_log.info("get Challenges by user");			
					challenges = CLSChallengeLocalServiceUtil.getChallengesForMeByUserId(user.getUserId(), 3, loc);
			    }
			    catch(Exception e){
			    	e.printStackTrace();
			    	_log.info("Exception geting Ideas Needs Artefacts or Challenges by user:"+e.getMessage());
			    	
			    }
		    
				if (ideas == null || ideas.length()==0){
					_log.info("userHasIdeas: "+false);
					userHasIdeas = false;
					if (pilot != null && pilot !=""){
						_log.info("getIdeasPilot");
						ideas = CLSIdeaLocalServiceUtil.getTopRatedIdeasOrNeedsByPilot(pilot, false, 3,loc);
						if (ideas!=null && ideas.length()>0) {
							_log.info("pilotHasIdeas: "+true);
							pilotHasIdeas = true;
						}
						else {
							_log.info("getIdeasLanguage");
							ideas = CLSIdeaLocalServiceUtil.getTopRatedIdeasOrNeedsByLanguage(languaje, false, 3,loc);
							if (ideas!=null && ideas.length()>0){
								_log.info("langHasIdeas: "+true);
								langHasIdeas = true;
							}
							
						}
					}
					else{
						_log.info("getIdeasLanguage");
						ideas = CLSIdeaLocalServiceUtil.getTopRatedIdeasOrNeedsByLanguage(languaje, false, 3,loc);
						if (ideas!=null && ideas.length()>0) langHasIdeas = true;
					}
				}
				else{
					_log.info("userHasIdeas: "+true);
					userHasIdeas = true;
				}
				if (challenges == null || challenges.length()==0){
					_log.info("userHasChallenges: "+false);
					userHasChallenges = false;
					if (pilot != null && pilot !=""){
						_log.info("getChallengesPilot");
						challenges = CLSChallengeLocalServiceUtil.getTopRatedChallengesByPilot(pilot, 3,loc);
						if (challenges!=null && challenges.length()>0) {
							_log.info("pilotHasChallenges: "+true);
							pilotHasChallenges = true;
						}
						else {
							_log.info("getChallengeLanguage");
							challenges = CLSChallengeLocalServiceUtil.getTopRatedChallengesByLanguage(languaje, 3,loc);
							if (challenges!=null && challenges.length()>0){
								_log.info("langHasChallenges: "+true);
								langHasChallenges = true;
							}
						}
	
					}
					else{
						_log.info("getChallengeLanguage");
						challenges =CLSChallengeLocalServiceUtil.getTopRatedChallengesByLanguage(languaje, 3,loc);
						if (challenges!=null && challenges.length()>0) {
							langHasChallenges = true;
						}
					}
				}
				else{
					_log.info("userHasChallenges: "+true);
					userHasChallenges = true;
				}
				
				if (needs == null || needs.length()==0){
					_log.info("userHasNeeds: "+false);
					userHasNeeds = false;
					if (pilot != null && pilot !=""){
						_log.info("getneedspilot");
						needs = CLSIdeaLocalServiceUtil.getTopRatedIdeasOrNeedsByPilot(pilot, true, 3,loc);
						if (needs!=null && needs.length()>0) {
							_log.info("pilotHasNeeds: "+true);
							pilotHasNeeds = true;
						}
						else {
							_log.info("getneedsLanguage");
							needs = CLSIdeaLocalServiceUtil.getTopRatedIdeasOrNeedsByLanguage(languaje, true, 3,loc);
							if (needs!=null && needs.length()>0){
								_log.info("langHasNeeds: "+true);
								langHasNeeds = true;
							}						
						}
	
					}
					else{
						_log.info("getneedsLanguage");
						needs = CLSIdeaLocalServiceUtil.getTopRatedIdeasOrNeedsByLanguage(languaje, true, 3,loc);
						if (needs!=null && needs.length()>0){
							_log.info("langHasNeeds: "+true);
							langHasNeeds = true;
						}
					}
			    }
				else {
					_log.info("userHasNeeds: "+true);
					userHasNeeds = true;
				}
				if (artifacts == null || artifacts.length()==0){
					_log.info("userHasArtefacts: "+false);
					userHasArtefacts = false;
					if (pilot != null && pilot !=""){
						_log.info("getArtefattopilot");
						artifacts = ArtefattoLocalServiceUtil.getTopRatedArtefactsByPilot(pilot, 3);
						if (artifacts!=null && artifacts.length()>0){
							_log.info("pilotHasArtefacts: "+true);
							pilotHasArtefacts = true;
						}
						else {
							_log.info("getArtefattoLanguage");
							artifacts = ArtefattoLocalServiceUtil.getTopRatedArtefactsByLanguage(languaje, 3);
							if (artifacts!=null && artifacts.length()>0) {
								_log.info("langHasArtefacts: "+true);
								langHasArtefacts = true;
							}
						}
						
					}
					else{
						artifacts = ArtefattoLocalServiceUtil.getTopRatedArtefactsByLanguage(languaje, 3);
						if (artifacts!=null && artifacts.length()>0) {
							_log.info("langHasArtefacts: "+true);
							langHasArtefacts = true;
						}
					}
			    }
				else{
					_log.info("userHasArtefacts: "+true);
					userHasArtefacts = true;
				}
		    }
		}
		else if (!debug){
			isLoged = false;
			//HttpServletRequest httpReq = PortalUtil.getOriginalServletRequest(request); 
			pilot = pilotr;
			if (pilot == null || pilot ==""){
			//If the user is not logged --> search pilot in session
				PortletSession sessionUser = renderRequest.getPortletSession();
				pilot = (String) sessionUser.getAttribute("LIFERAY_SHARED_pilot", PortletSession.APPLICATION_SCOPE);
			}
			if (pilot == null || pilot ==""){
				pilotHasArtefacts = false;
				pilotHasIdeas = false;
				pilotHasNeeds = false;
				pilotHasChallenges = false;
				//If not user logged and no pilot in session we get the ideas and needs from languaje.
				//ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
				pilot ="";
				ideas = CLSIdeaLocalServiceUtil.getTopRatedIdeasOrNeedsByLanguage(languaje, false, 3,loc);
				if (ideas == null || ideas.length()==0){
					langHasIdeas = false;
				}
				else{
					langHasIdeas = true;
				}
				challenges =CLSChallengeLocalServiceUtil.getTopRatedChallengesByLanguage(languaje, 3,loc);
				if (challenges == null || challenges.length()==0){
					langHasChallenges = false;
				}
				else{
					langHasChallenges = true;
				}
				
				needs = CLSIdeaLocalServiceUtil.getTopRatedIdeasOrNeedsByLanguage(languaje, true, 3,loc);
				if (needs == null || needs.length()==0){
					langHasNeeds = false;
				}
				else{
					langHasNeeds = true;
				}
				
				artifacts = ArtefattoLocalServiceUtil.getTopRatedArtefactsByLanguage(languaje, 3);
				if (artifacts == null || artifacts.length()==0){
					langHasArtefacts = false;
				}
				else{
					langHasArtefacts = true;
				}
				
			}
			else{
				ideas = CLSIdeaLocalServiceUtil.getTopRatedIdeasOrNeedsByPilot(pilot, false, 3,loc);
				if (ideas != null && ideas.length() > 0){
					pilotHasIdeas = true;
				}
				else{
					pilotHasIdeas = false;
					ideas = CLSIdeaLocalServiceUtil.getTopRatedIdeasOrNeedsByLanguage(languaje, false, 3,loc);
					if (ideas == null || ideas.length()==0){
						langHasIdeas = false;
					}
					else{
						langHasIdeas = true;
					}
					
				}
				challenges = CLSChallengeLocalServiceUtil.getTopRatedChallengesByPilot(pilot, 3,loc);
				if (challenges != null && challenges.length() > 0){
					pilotHasChallenges = true;
				}
				else{
					pilotHasChallenges = false;
					challenges =CLSChallengeLocalServiceUtil.getTopRatedChallengesByLanguage(languaje, 3,loc);
					if (challenges == null || challenges.length()==0){
						langHasChallenges = false;
					}
					else{
						langHasChallenges = true;
					}
					
				}
				
				needs = CLSIdeaLocalServiceUtil.getTopRatedIdeasOrNeedsByPilot(pilot, true, 3,loc);
				if (needs != null && needs.length() > 0){
					pilotHasNeeds = true;
				}
				else{
					pilotHasNeeds = false;
					needs = CLSIdeaLocalServiceUtil.getTopRatedIdeasOrNeedsByLanguage(languaje, true, 3,loc);
					if (needs == null || needs.length()==0){
						langHasNeeds = false;
					}
					else{
						langHasNeeds = true;
					}
					
				}
				
				artifacts = ArtefattoLocalServiceUtil.getTopRatedArtefactsByPilot(pilot, 3);
				if (artifacts != null && artifacts.length() > 0){
					pilotHasArtefacts = true;
				}
				else{
					pilotHasArtefacts = false;
					artifacts = ArtefattoLocalServiceUtil.getTopRatedArtefactsByLanguage(languaje, 3);
					if (artifacts == null || artifacts.length()==0){
						langHasArtefacts = false;
					}
					else{
						langHasArtefacts = true;
					}
					
				}				
			}
		}

	} catch (Exception e) {
		e.printStackTrace();
		_log.error("Exception"+e.getMessage());
	}
	try{
	    if(themeDisplay.getPermissionChecker().isOmniadmin()){
	    	role+="-OnmiAdmin";
	    }
		if (pilot.equals("Bilbao")){
			pilotforchart = "['Bilbao','Bilbao', 'Bilbao', 'Bilbao']";
		}
		if (pilot.equals("Trento")){
			pilotforchart = "['Trento', 'Trento', 'Trento', 'Trento']";
		}
		if (pilot.equals("Novisad")){
			pilotforchart = "['Novisad','Novisad', 'Novisad', 'Novisad']";
		}
		if (pilot.equals("Uusimaa")){
			pilotforchart = "['Uusimaa', 'Uusimaa', 'Uusimaa', 'Uusimaa']";
		} 			
	
	} 
	catch (Exception e) {
		e.printStackTrace();
		_log.error("Exception"+e.getMessage());
	}	
	int nView = 0;
	String contextPath = request.getContextPath();
	String imgPath = contextPath + "/img";
%>
<script>
var pilotforchart=<%=pilotforchart %>;
var pilot ='<%=pilot %>';
var pilotr ='<%=pilotr %>';
var isLoged = <%=isLoged%>;
var role='<%=role %>';
var ccUserId=<%=ccUserId%>;
var baseurl='<%=baseurl%>';
var istest=<%=test%>;
var userHasIdeas =<%=userHasIdeas%>;
var locale='<%=languaje%>';
var isSimpleUser = <%=isSimpleUser%>;
var debug=<%=debug%>;
console.log('debug:'+debug);
console.log('pilot:'+pilot);
console.log('locale:'+locale);
console.log('pilotforchart:'+pilotforchart);
console.log('isSimpleUser:'+isSimpleUser);

console.log('pilotr:'+pilotr);
console.log('ccUserId:'+ccUserId);
console.log('baseurl:'+baseurl);
console.log('User role:'+role+' isAuthority:'+<%=isAuthority%>+' isDeveloper:'+<%=isDeveloper%>+' isCompanyLeader:'+<%=isCompanyLeader%>+' isCitizen:'+<%=isCitizen%>+' isAcademy:'+<%=isAcademy%>+ 'isBussines:'+<%=isBussines%>);
console.log('userHasIdeas:'+<%=userHasIdeas%>+' pilotHasIdeas:'+<%=pilotHasIdeas%>+' langHasIdeas:'+<%=langHasIdeas%>);
console.log('userHasNeeds:'+<%=userHasNeeds%>+' pilotHasNeeds:'+<%=pilotHasNeeds%>+' langHasNeeds:'+<%=langHasNeeds%>);
console.log('userHasChallenges:'+<%=userHasChallenges%>+' pilotHasChallenges:'+<%=pilotHasChallenges%>+' langHasChallenges:'+<%=langHasChallenges%>);
console.log('userHasArtefacts:'+<%=userHasArtefacts%>+' pilotHasArtefacts:'+<%=pilotHasArtefacts%>+' langHasArtefacts:'+<%=langHasArtefacts%>);
console.log('needs:');
console.log(<%=needs%>);
console.log('ideas:');
console.log(<%=ideas%>);
console.log('challenges:');
console.log(<%=challenges%>);
console.log('artifacts:');
console.log(<%=artifacts%>);

var nextPage=null;

function selectPage(target){
	console.log(target);
	nextPage=target;
	if (window.sessionStorage.pilot){
		nextPage+='&'+'<portlet:namespace/>pilot='+window.sessionStorage.pilot;
		document.location=nextPage;
	}else{
		document.getElementById("selector").style.display='block';
		changePageNow=true;
	}
}
function goToRegister(target){
	console.log(target);
	nextPage=target;
	if (window.sessionStorage.pilot){
		nextPage+='&'+'<portlet:namespace/>pilot='+window.sessionStorage.pilot;
	}
	document.location=nextPage;
	
}
var changePageNow=true;

function changePilot(){
	document.getElementsByClassName("selectedPilot")[0].style.display='none';
	document.getElementById("selector").style.display='block';	
	window.sessionStorage.removeItem("pilot");
	changePageNow=false;
}

function changePage(pilot){
	nextPage+='&'+'<portlet:namespace/>pilot='+pilot;
	if (changePageNow==true){
		console.log(nextPage);
		document.location=nextPage;
	}else{
		changePageNow=true;
	}
}

function cambiarPortada(){
	if(window.sessionStorage.pilot){
		//document.getElementsByClassName("bloque1")[0].style.backgroundImage="url('<%=imgPath%>/controller/"+window.sessionStorage.pilot+".jpg')";
		//document.getElementsByClassName("selectedPilot")[0].style.display='block';
	}else{
		var portadas=["<%=Constants.PILOT_CITY_BILBAO%>","<%=Constants.PILOT_CITY_HELSINKI%>","<%=Constants.PILOT_CITY_NOVISAD%>","<%=Constants.PILOT_CITY_TRENTO%>"]
		var num=new Date().getTime()%4;
//		document.getElementsByClassName("bloque1")[0].style.backgroundImage="url('<%=imgPath%>/controller/"+portadas[num]+".jpg')";
//		document.getElementsByClassName("selectedPilot")[0].style.display='none';
	}
}

function scroolTo(obj){
	$('html, body').animate({
	    scrollTop: document.getElementById(obj).offsetTop-40
	}, 1000);
}
//setInterval(function(){ cambiarPortada(); }, 3000);

function ShowPilotMenu(){
	document.getElementById('divPilotSelect').style.left=($('#logo-container').position().left+5)+'px';
	document.getElementById("divPilotSelect").style.display='block';	
	
}

function closePilotMenu(pilot){
	document.getElementById("divPilotSelect").style.display='none';	
	//document.getElementById("textPilot").innerHTML=pilot;
	//document.getElementsByClassName("bloque1")[0].style.backgroundImage="url('<%=imgPath%>/controller/"+window.sessionStorage.pilot+".jpg')";
	window.sessionStorage.pilot=pilot;
	
}

var fakeMarketplaceData=<%=artifacts%>;
if (fakeMarketplaceData == null || fakeMarketplaceData.length ==0){
 fakeMarketplaceData=[];
}
var fakeNeedData=<%=needs%>;

if (fakeNeedData == null || fakeNeedData.length ==0){

	  fakeNeedData=[];
}

var fakeChallengeData=<%=challenges%>;

if (fakeChallengeData == null || fakeChallengeData.length ==0){

	 fakeChallengeData=[];
}




var fakeIdeaData=<%=ideas%>;

if (fakeIdeaData == null || fakeIdeaData.length ==0){
	

	 fakeIdeaData=[];
}

function llenarOpinion(){
	var contenedor= document.getElementById("cityopinion");
	if(sessionStorage.pilot!=null){
		
		if (sessionStorage.pilot=="<%=URLEncoder.encode(Constants.PILOT_CITY_BILBAO, "UTF-8")%>"){
			contenedor.innerHTML=''+
			'<div class="izquierda seccion textoCursiva">'+
			'"WeLive gives us an opportunity to challenge IT entrepreneurs to provide solutions based on the best ideas given by our citizens and their views of the future of our city. This project is of great importance for our city to become Smart City - the most favorable place for living and doing business in Serbia and The European Capital of Culture for 2021."'+
			'</div>'+
			'<div class="izquierda seccion textoNormal textoPerfil">'+
				'<img class="fotoPerfil" src="<%=imgPath%>/controller/gsecujski.jpg">'+
				'Mr. Goran Sečujski, Head of the Local Economic Development Office, the City of Novi Sad'+
			'</div>';

		}
		else if (sessionStorage.pilot=="<%=URLEncoder.encode(Constants.PILOT_CITY_NOVISAD, "UTF-8")%>"){
			contenedor.innerHTML=''+
			'<div class="izquierda seccion textoCursiva">'+
			'"WeLive gives us an opportunity to challenge IT entrepreneurs to provide solutions based on the best ideas given by our citizens and their views of the future of our city. This project is of great importance for our city to become Smart City - the most favorable place for living and doing business in Serbia and The European Capital of Culture for 2021."'+
			'</div>'+
			'<div class="izquierda seccion textoNormal textoPerfil">'+
				'<img class="fotoPerfil" src="<%=imgPath%>/controller/gsecujski.jpg">'+
				'Mr. Goran Sečujski, Head of the Local Economic Development Office, the City of Novi Sad'+
			'</div>';

		}
		else if (sessionStorage.pilot=="<%=URLEncoder.encode(Constants.PILOT_CITY_HELSINKI, "UTF-8")%>"){
			contenedor.innerHTML=''+
			'<div class="izquierda seccion textoCursiva">'+
			'"WeLive gives us an opportunity to challenge IT entrepreneurs to provide solutions based on the best ideas given by our citizens and their views of the future of our city. This project is of great importance for our city to become Smart City - the most favorable place for living and doing business in Serbia and The European Capital of Culture for 2021."'+
			'</div>'+
			'<div class="izquierda seccion textoNormal textoPerfil">'+
				'<img class="fotoPerfil" src="<%=imgPath%>/controller/gsecujski.jpg">'+
				'Mr. Goran Sečujski, Head of the Local Economic Development Office, the City of Novi Sad'+
			'</div>';

		}		
		else if (sessionStorage.pilot=="<%=URLEncoder.encode(Constants.PILOT_CITY_TRENTO, "UTF-8")%>"){
			contenedor.innerHTML=''+
			'<div class="izquierda seccion textoCursiva">'+
			'"WeLive gives us an opportunity to challenge IT entrepreneurs to provide solutions based on the best ideas given by our citizens and their views of the future of our city. This project is of great importance for our city to become Smart City - the most favorable place for living and doing business in Serbia and The European Capital of Culture for 2021."'+
			'</div>'+
			'<div class="izquierda seccion textoNormal textoPerfil">'+
				'<img class="fotoPerfil" src="<%=imgPath%>/controller/gsecujski.jpg">'+
				'Mr. Goran Sečujski, Head of the Local Economic Development Office, the City of Novi Sad'+
			'</div>';

		}		
		else {
			contenedor.innerHTML=''+
			'<div class="izquierda seccion textoCursiva">'+
			'"WeLive gives us an opportunity to challenge IT entrepreneurs to provide solutions based on the best ideas given by our citizens and their views of the future of our city. This project is of great importance for our city to become Smart City - the most favorable place for living and doing business in Serbia and The European Capital of Culture for 2021."'+
			'</div>'+
			'<div class="izquierda seccion textoNormal textoPerfil">'+
				'<img class="fotoPerfil" src="<%=imgPath%>/controller/gsecujski.jpg">'+
				'Mr. Goran Sečujski, Head of the Local Economic Development Office, the City of Novi Sad'+
			'</div>';

		}				
	}
	else {
		contenedor.innerHTML=''+
		'<div class="izquierda seccion textoCursiva">'+
		'"WeLive gives us an opportunity to challenge IT entrepreneurs to provide solutions based on the best ideas given by our citizens and their views of the future of our city. This project is of great importance for our city to become Smart City - the most favorable place for living and doing business in Serbia and The European Capital of Culture for 2021."'+
		'</div>'+
		'<div class="izquierda seccion textoNormal textoPerfil">'+
			'<img class="fotoPerfil" src="<%=imgPath%>/controller/gsecujski.jpg">'+
			'Mr. Goran Sečujski, Head of the Local Economic Development Office, the City of Novi Sad'+
		'</div>';

	}					
	
}
function llenarTarjeta(jsondata, contenedor){
	
	if (jsondata != null && jsondata.length > 0){
		var contenedor= document.getElementById(contenedor);
		var contenido='';
		for(var i=0;i<jsondata.length; i++){
			contenido+=getElemTarjeta(jsondata[i]);
		}
		contenedor.innerHTML=contenido;
	}
}
function llenarTarjetaNeeds(jsondata, contenedor){
	
	if (jsondata != null && jsondata.length > 0){
		var contenedor= document.getElementById(contenedor);
		var contenido='';
		for(var i=0;i<jsondata.length; i++){
			contenido+=getElemTarjetaNeeds(jsondata[i]);
		}
		contenedor.innerHTML=contenido;
	}
}

function llenarTarjetaChallenges(jsondata, contenedor){
	
	if (jsondata != null && jsondata.length > 0){
		var contenedor= document.getElementById(contenedor);
		var contenido='';
		for(var i=0;i<jsondata.length; i++){
			contenido+=getElemTarjetaChagenge(jsondata[i]);
		}
		contenedor.innerHTML=contenido;
	}
}


function llenarTarjetaIdeas(jsondata, contenedor){
	
	if (jsondata != null && jsondata.length > 0){
		var contenedor= document.getElementById(contenedor);
		var contenido='';
		for(var i=0;i<jsondata.length; i++){
			contenido+=getElemTarjetaIdeas(jsondata[i]);
		}
		contenedor.innerHTML=contenido;
	}
}
/*
 * "vocabularyName":"Environment & Energy",
 "text":"I think the LED lamps could replace the old technology illumination system",
 "vocabularyIcon":"icon-leaf",
 "status":"selected",
 "statusi18n":"Selected",
 "referredTo":"Trento Municipality",
 "image":"http://localhost:8080/Challenge62-portlet/img/ideasImg.png","themes":"Waste",
 "tit":"LED lamps on the market",
 "type":"Idea",
 "url":"http://localhost:8080/ideas_explorer/-/ideas_explorer_contest/18701/view",
 "creationDate":"09/12/2016",
 "isTakenUp":false,
 "vocabularyColor":"green",
 "authorName":"Chiara Capizzi",
  "referredToIcon":"location_city",
 "rating":4,
 "associatedToChallenge":true,
 "associatedChallengeURL":"http://localhost:8080/challenges_explorer/-/challenges_explorer_contest/5101/view",
 "associatedChallengeName":"Christamas Market energy consumption",
 "associatedToNeed":true
 "associatedNeedName":"Need title",
 "associatedNeedURL":"http://localhost:8080/needs_explorer/-/needs_explorer_contest/20703/view",

 */
function getElemTarjetaIdeas(obj){
	var stars='';
	for (i=0;i<5;i++){
		if (obj.rating > i) stars = stars+ '<i class="icon-star"></i>';
		else stars = stars+ '<i class="icon-star-empty"></i>';
	}
 	var titleneedorchallenge="";
 	var url="";
 	var name="";
 	if (obj.associatedToChallenge){
 		titleneedorchallenge ='<liferay-ui:message key="overlay.cardidea.associatedchallenge" />';
 		url=obj.associatedChallengeURL;
 		name=obj.associatedChallengeName;
 	}
 	else if (obj.associatedToNeed){
 		titleneedorchallenge ='<liferay-ui:message key="overlay.cardidea.associatedneed" />';
 		url=obj.associatedNeedURL;
 		name=obj.associatedNeedName;
 		
 	}
 	else{
 		titleneedorchallenge ='<liferay-ui:message key="overlay.cardidea.associatedchallenge" />';
 		url="";
 		name= '<liferay-ui:message key="overlay.cardidea.nochallenge" />';
 	}
 	/*
 	Then for idea cards:
 		elaboration –> selection
		evaluation  evaluation
		selected  selected
		refinement  refinement
		implementation  implementation
		monitoring  monitoring
 	*/
 	var selectionElaboration ="grey-text";
 	var selectionEvaluation ="grey-text";
 	var selectionSelected ="grey-text";
 	var selectionRefinement="grey-text";
 	var selectionImplementation ="grey-text";
 	var selectionMonitoring ="grey-text";
 	
 	if (obj.status == 'selection'){
 		selectionElaboration = "white-text";
 	}
 	else if (obj.status == 'evaluation'){
  		selectionEvaluation ="white-text";
 	}
 	else if (obj.status == 'selected'){
 		selectionSelected ="white-text";
 	}
 	else if (obj.status == 'refinement'){
 		selectionRefinement ="white-text";
 	}
 	else if (obj.status == 'implementation'){
 		selectionImplementation ="white-text";
 	}
 	else if (obj.status == 'monitoring'){
		selectionMonitoring ="white-text";
 	}
	
	retorno=''+
	'<div class="div33">'+
	'<div class="tarjeta" >'+
	' 	<div class="cabeceraTarjeta '+obj.vocabularyColor+'">'+
	'       <div class="cabeceratarjeta16"><i class="material-icons '+ obj.vocabularyIcon+'"></i></div>'+
	'       <div class="cabeceratarjeta66 textellipsis"><span class="truncar tooltipped" data-position="bottom" data-delay="50" data-tooltip="'+obj.vocabularyName+'">'+obj.vocabularyName+'</span></div>'+
	'		<div style="display: inline-block;"><a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardidea.view-idea" />" href="'+obj.url+'"> <i class="material-icons" style="color: white;">more_vert</i> </a></div>'+
	'   </div>'+
	'   <div class="card-content">'+
	'   	<div class="bottom-margin">'+
	'			<p class="card-multi-title">'+obj.tit+'<a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardidea.view-idea" />" href="'+obj.url+'"> <i class="material-icons fa-fw fa-lg" style="color: #6fafda;">more</i> </a>'+
	'           </p>'+
	'           <p class="grey-text text-darken-2 ">'+ 
	'           <label class="grey-text text-darken-18"><liferay-ui:message key="overlay.cardidea.submitted-by" /></label>'+
	'           <span class="grey-text text-darken-16" >'+obj.authorName+'</span>'+ 
	'			</p> '+
	'        </div>'+	
	'	     <div class="divider "></div>'+	
	'        <div class="section paddingAll sectionCardTags">'+
	'              <span class="truncar">'+
	'                    <i class="material-icons icon-asterisk tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardidea.themes" />"></i>'+ 
	'                    <span class="grey-text" style="font-size: 14px;"><small>'+obj.themes+'</small></span>'+
	'              </span>'+
	'        </div>'+
	'	    <div class="divider"></div>'+
	'		<div>'+ 
	'            <div class="divcard50">'+
	'      		 	<h6 class="weight-5"><liferay-ui:message key="overlay.cardidea.creation-date" /></h6> <span class="dataCreazioneIdea" style="font-size: 14px;">'+obj.creationDate+'</span>'+
	'           </div>'+ 
	'           <div class="divcard50">'+
	'               <h6 class="weight-5"><liferay-ui:message key="overlay.cardidea.ratings" /></h6>'+
	'               <span class="votoMedio material-icons">'+stars+
	'               </span>'+
	'           </div>'+
	'           <div >'+
	'               <h6 class="valign-wrapper"> <i class="material-icons small tooltipped" data-position="top" data-delay="50" data-tooltip="Authority" >location_city</i> &nbsp; <span class="enteIdea" style="font-size: 14px;">'+obj.referredTo+'</span> </h6>'+
	'           </div>'+ 
	'       </div>'+
	'	    <div class="divider"></div>'+
	' 		<div style="margin-bottom: 20px; height:120px;overflow:hidden;">'+ 
    '            <div class="cabeceratarjeta33img" style="float:left;" ><img src="'+obj.image+'"/></div>'+
	'		     <div class="cuerpotarjeta66" style="height: 100px;overflow: hidden;"><p style="font-size: 14px;">'+obj.text+'</p></div>'+	
	'	    </div>'+
	'	    <div class="divider"></div>'+
	' 		<div class="textellipsis" style="margin-bottom: 20px; max-width: 90%;">'+ 
	'               <h6 class="weight-5">'+titleneedorchallenge+'</h6>'+
	'               <span class="material-icons">';
	if (obj.associatedToChallenge || obj.associatedToNeed){
		retorno=retorno+'                     <a class="linkazul" href="'+url+'" target="_blank"><span>'+name+'</span>&nbsp;<i class="icon-external-link"></i></a>';
		
	}
	else{
		retorno=retorno+'                     <span style="font-size: 14px;">'+name+'</span>';
		
	}
	retorno=retorno+'               </span>'+
	'	    </div>'+	    
	'   </div>'+ 
	'  <div class="footer-card center-align grey darken-2 white-text ideasFlow">'+
    '  	  <span class="material-icons iconaStato selection icon-lightbulb tooltipped '+selectionElaboration+'" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardidea.elaboration-phase" />"></span>&nbsp;'+
	'	  <span class="small iconaStato icon-angle-right"></span>&nbsp;'+
    '     <span class="material-icons  iconaStato evaluation '+selectionEvaluation+' tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardidea.evaluation-phase" />" data-tooltip-id="04b782e5-56e1-c672-fcb1-8c9ccb9626a1">gavel</span>&nbsp;'+
	'     <span class="small iconaStato icon-angle-right"></span>&nbsp;'+
	'	  <span class="material-icons  iconaStato selected '+selectionSelected+' tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardidea.selected-phase" />" data-tooltip-id="ae99770f-14f2-64c3-ffb0-53774bb179ce">check_circle</span>&nbsp;'+
	'     <span class="small iconaStato icon-angle-right"></span>&nbsp;'+
	' 	  <span class="material-icons  iconaStato refinement icon-retweet '+selectionRefinement+' tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardidea.refinement-phase" />" data-tooltip-id="dcca6f36-b1fc-73df-46d9-8d971f5a5196"></span>&nbsp;'+
	'     <span class="small iconaStato icon-angle-right "></span>&nbsp;'+
	'	  <span class="material-icons  iconaStato implementation icon-cogs '+selectionImplementation+' tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardidea.implementation-phase" />" data-tooltip-id="80fafe95-d4fb-d41a-d8c5-c242e767fe0a"></span>&nbsp;'+
	'     <span class="small iconaStato icon-angle-right"></span>&nbsp;'+
	'	  <span class="material-icons  iconaStato monitoring icon-desktop '+selectionMonitoring+' tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardidea.monitoring-phase" />" data-tooltip-id="076289ca-58fd-e5a2-fb15-6b5492e3ccf8"></span>&nbsp;'+      	
    '      </div>'+
	'</div>'+
	'</div>';
	return retorno;
	
	
}
/**
 * [{"vocabularyName":"Strutture",
	 "startDate":"21/06/2017",
	 "text":"wds",
	 "vocabularyIcon":"icon-building",
	 "status":"open",
	 "image":"http://localhost:8080/Challenge62-portlet/img/challengesImg.png",
	 "tit":"Sfida test categorie",
	 "type":"Challenge",
	 "url":"http://localhost:8080/challenges_explorer/-/challenges_explorer_contest/6201/view",
	 "numIdeas":1,
	 "vocabularyColor":"grey",
	 "authorName":"Municipality Trento",
	 "endtDate":"22/07/2017","statusi18n":"Aperta",
	 "rating":0,"authorOrganizationName":"Trento Municipality"}
 */
function getElemTarjetaChagenge(obj){
	var stars='';
	for (i=0;i<5;i++){
		if (obj.rating > i) stars = stars+ '<i class="icon-star"></i>';
		else stars = stars+ '<i class="icon-star-empty"></i>';
	}
 	
 	var titleneedorchallenge ='<liferay-ui:message key="overlay.cardidea.associatedneed" />';
 		
 	
 	/*
 	Then for idea cards:
 		elaboration –> selection
		evaluation  evaluation
		selected  selected
		refinement  refinement
		implementation  implementation
		monitoring  monitoring
 	*/
 	var selectionOpen ="grey-text";
 	var selectionInEvaluation ="grey-text";
 	var selectionEvaluated ="grey-text";
 	
 	if (obj.status == 'open'){
 		selectionOpen = "white-text";
 	}
 	else if (obj.status == 'evaluation'){
 		selectionInEvaluation ="white-text";
 	}
 	else if (obj.status == 'evaluated'){
 		selectionEvaluated ="white-text";
 	}
 	
	
	retorno=''+
	'<div class="div33">'+
	'<div class="tarjeta" >'+
	' 	<div class="cabeceraTarjeta '+obj.vocabularyColor+'">'+
	'       <div class="cabeceratarjeta16"><i class="material-icons '+ obj.vocabularyIcon+'"></i></div>'+
	'       <div class="cabeceratarjeta66 textellipsis"><span class="truncar tooltipped" data-position="bottom" data-delay="50" data-tooltip="'+obj.vocabularyName+'">'+obj.vocabularyName+'</span></div>'+
	'		<div style="display: inline-block;"><a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardchallenge.view-challenge" />" href="'+obj.url+'"> <i class="material-icons" style="color: white;">more_vert</i> </a></div>'+
	'   </div>'+
	'   <div class="card-content">'+
	'   	<div class="bottom-margin">'+
	'			<p class="card-multi-title">'+obj.tit+'<a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardchallenge.view-challenge" />" href="'+obj.url+'"> <i class="material-icons fa-fw fa-lg" style="color: #6fafda;">more</i> </a>'+
	'           </p>'+
	'           <p class="grey-text text-darken-2 ">'+ 
	'           <label class="grey-text text-darken-18"><liferay-ui:message key="overlay.cardidea.submitted-by" /></label>'+
	'           <span class="grey-text text-darken-16" >'+obj.authorName+'</span>'+ 
	'			</p> '+
	'        </div>'+	
	'	     <div class="divider "></div>'+	
	'		<div>'+ 
	'            <div class="divcard50">'+
	'      		 	<h6 class="weight-5"><liferay-ui:message key="overlay.cardchallenge.init-date" /></h6> <span class="dataCreazioneIdea" style="font-size: 14px;">'+obj.startDate+'</span>'+
	'           </div>'+ 
	'            <div class="divcard50">'+
	'      		 	<h6 class="weight-5"><liferay-ui:message key="overlay.cardchallenge.end-date" /></h6> <span class="dataCreazioneIdea" style="font-size: 14px;">'+obj.endtDate+'</span>'+
	'           </div>'+
	//'           <div>'+
	//'               <span class="votoMedio material-icons" style="text-align: center;">'+stars+
	//'               </span>'+
	//'           </div>'+ 		
	'       </div>'+
	'	    <div class="divider"></div>'+
	' 		<div style="margin-bottom: 20px; height:120px;overflow:hidden;">'+ 
    '            <div class="cabeceratarjeta33img" style="float:left;" ><img src="'+obj.image+'"/></div>'+
	'		     <div class="cuerpotarjeta66" style="height: 100px;overflow: hidden;"><p style="font-size: 14px;">'+obj.text+'</p></div>'+	
	'	    </div>'+
	'	    <div class="divider"></div>'+
	' 		<div class="textellipsis" style="margin-bottom: 20px; max-width: 90%;">'+ 
	'				<i class="material-icons icon-lightbulb small">&nbsp;</i>'+
	'               <span style="font-size: 14px;"><liferay-ui:message key="overlay.cardchallenge.proposed-ideas" />:&nbsp</span>'+
    '               <span style="font-size: 14px;">'+obj.numIdeas+'</span>'+
	'	    </div>'+	    
	'   </div>'+ 
	'  <div class="footer-card center-align grey darken-2 white-text ideasFlow">'+
    '  	  <span class="material-icons iconaStato selection icon-lightbulb tooltipped '+selectionOpen+'" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardidea.elaboration-phase" />"></span>&nbsp;'+
	'	  <span class="small iconaStato icon-angle-right"></span>&nbsp;'+
    '     <span class="material-icons  iconaStato evaluation '+selectionInEvaluation+' tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardidea.evaluation-phase" />" data-tooltip-id="04b782e5-56e1-c672-fcb1-8c9ccb9626a1">gavel</span>&nbsp;'+
	'     <span class="small iconaStato icon-angle-right"></span>&nbsp;'+
	'	  <span class="material-icons  iconaStato selected '+selectionEvaluated+' tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardidea.selected-phase" />" data-tooltip-id="ae99770f-14f2-64c3-ffb0-53774bb179ce">check_circle</span>&nbsp;'+
   '      </div>'+
	'</div>'+
	'</div>';
	return retorno;
	
	
}




/**
 * "vocabularyName":"Economy",
 "text":"Descrizione della segnalazione numero 1 a",
 "vocabularyIcon":"icon-money",
 "referredTo":"Trento Municipality",
 "image":"http://localhost:8080/Challenge62-portlet/img/needsImg.png",
 "themes":"Innovation Ecosystem,&nbsp;Technology & Innovation",
 "tit":"Fil need 1",
 "type":"Need",
 "url":"http://localhost:8080/needs_explorer/-/needs_explorer_contest/20401/view",
 "creationDate":"14/03/2017",
 "vocabularyColor":"teal",
 "authorName":"Filippo Giuffrida",
 "rating":0,
 "numberOfLinkedChallenge":0,
 "numberOfProposedIdeas":0
 */
function getElemTarjetaNeeds(obj){
	 
	var stars='';
	for (i=0;i<5;i++){
		if (obj.rating > i) stars = stars+ '<i class="icon-star"></i>';
		else stars = stars+ '<i class="icon-star-empty"></i>';
	}
	retorno=''+
	'<div class="div33">'+
	'<div class="tarjeta" >'+
	' 	<div class="cabeceraTarjeta '+obj.vocabularyColor+'">'+
	'       <div class="cabeceratarjeta16"><i class="material-icons '+ obj.vocabularyIcon+'"></i></div>'+
	'       <div class="cabeceratarjeta66 textellipsis"><span class="truncar tooltipped" data-position="bottom" data-delay="50" data-tooltip="'+obj.vocabularyName+'">'+obj.vocabularyName+'</span></div>'+
	'		<div style="display: inline-block;"><a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardneed.view-need" />" href="'+obj.url+'"> <i class="material-icons" style="color: white;">more_vert</i> </a></div>'+
	'   </div>'+
	'   <div class="card-content">'+
	'   	<div class="bottom-margin">'+
	'			<p class="card-multi-title">'+obj.tit+'<a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardneed.view-need" />" href="'+obj.url+'"> <i class="material-icons fa-fw fa-lg" style="color: #6fafda;">more</i> </a>'+
	'           </p>'+
	'           <p class="grey-text text-darken-2 ">'+ 
	'           <label class="grey-text text-darken-18"><liferay-ui:message key="overlay.cardidea.submitted-by" /></label>'+
	'           <span class="grey-text text-darken-16" >'+obj.authorName+'</span>'+ 
	'			</p> '+
	'        </div>'+	
	'	     <div class="divider "></div>'+	
	'        <div class="section paddingAll sectionCardTags">'+
	'              <span class="truncar">'+
	'                    <i class="material-icons icon-asterisk tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.cardidea.themes" />"></i>'+ 
	'                    <span class="grey-text" style="font-size: 14px;">&nbsp;<small>'+obj.themes+'</small></span>'+
	'              </span>'+
	'        </div>'+
	'	    <div class="divider"></div>'+
	'		<div>'+ 
	'            <div class="divcard50">'+
	'      		 	<h6 class="weight-5">Creation Date</h6> <span class="dataCreazioneIdea" style="font-size: 14px;">'+obj.creationDate+'</span>'+
	'           </div>'+ 
	'           <div class="divcard50">'+
	'               <h6 class="weight-5"><liferay-ui:message key="overlay.cardidea.ratings" /></h6>'+
	'               <span class="votoMedio material-icons" >'+stars+
	'               </span>'+
	'           </div>'+
	'           <div >'+
	'               <h6 class="valign-wrapper"> <i class="material-icons small tooltipped" data-position="top" data-delay="50" data-tooltip="Authority" >location_city</i> &nbsp; <span class="enteIdea" style="font-size: 14px;">'+obj.referredTo+'</span> </h6>'+
	'           </div>'+ 
	'       </div>'+
	'	    <div class="divider"></div>'+
	' 		<div style="display: table-row; margin-bottom: 20px; height:120px;overflow:hidden;">'+ 
    '            <div class="cabeceratarjeta33img" style="float:left;"><img src="'+obj.image+'"/></div>'+
	'		     <div class="cuerpotarjeta66" style="height: 100px;overflow: hidden;"><p style="font-size: 14px;">'+obj.text+'</p></div>'+	
	'	    </div>'+
	'	    <div class="divider"></div>'+
	' 		<div style="margin-bottom: 20px;">'+ 
    '            <div class="cuerpotarjeta16"><i class="material-icons icon-trophy small">&nbsp;</i></div>'+
	'		     <div class="cuerpotarjeta66"><p style="font-size: 14px;"><liferay-ui:message key="overlay.cardneed.number-of-proposed-ideas" />:'+obj.numberOfLinkedChallenge+'</p></div>'+	
	'	    </div>'+	    
	'	    <div class="divider"></div>'+
	' 		<div style="margin-bottom: 20px;">'+ 
    '            <div class="cuerpotarjeta16"><i class="material-icons  icon-lightbulb small">&nbsp;</i></div>'+
	'		     <div class="cuerpotarjeta66"><p style="font-size: 14px;"><liferay-ui:message key="overlay.cardneed.number-of-linked-challenges" />:'+obj.numberOfProposedIdeas+'</p></div>'+	
	'	    </div>'+
	'   </div>'+ 
	'   <div class="footer-card center-align grey darken-2 white-text"></div>'+

	'</div>'+
	'</div>';
	return retorno;
	
	
}

function getElemTarjeta(obj){
	retorno=''+
	'<div class="div33">'+
	'<div class="tarjeta" onclick="document.location=\''+obj.url+'\'">'+
	' 	<div class="cabeceraTarjeta green">'+
	//'       <div class="cabeceratarjeta16"><i class="material-icons '+ obj.vocabularyIcon+'"></i></div>'+
	'       <div class="cabeceratarjeta66 textellipsis"><span class="truncar tooltipped" data-position="bottom" data-delay="50" data-tooltip="'+obj.type+'">'+obj.type+'</span></div>'+
	'		<div style="display: inline-block;"><a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="View Service" href="'+obj.url+'"> <i class="material-icons" style="color: white;">more_vert</i> </a></div>'+
	'   </div>'+
	'	<div class="divider"></div>'+	
	'	<div style="background-image: url(\''+obj.image+'\');" class="imgTarjeta"></div>'+
	'	<div class="divider"></div>'+
	'	<div class="titTarjeta">'+obj.tit+'</div>'+
	'	<div class="divider"></div>'+
	'	<div class="textoTarjeta">'+obj.text+'</div>'+
	'	<div class="divider"></div>'+	
	'	<div class="LikesTarjeta"><span>'+obj.rating+'<span><img class="iconLike" src="<%=imgPath%>/controller/icon-star.png"></div>'+
	'</div>'+
	'</div>';
	return retorno;
	
	
}

function viewDeveloper(url){
	if (role.indexOf("OnmiAdmin")>-1||role.indexOf("Developer")>-1||role.indexOf("Authority")>-1){
		selectPage(url);
	}else{
		alert('You must be a "Developer" to access this tool.')
	}
}

var baseurl="<%=baseurl%>";
var kpidata={
		kpi14headerDATASETALL:{
			url:baseurl+"/analytics/oia/idea/search/all/materializedcitydelta",
			title:"Actual artefacts",
			type:"Bar2D",
			pilottype:0,
			color:"#bebf03",
			htmlid:"datasetPublished",
			id:"kpi14"
		},			
		kpi14headerDATASET:{
			url:baseurl+"/analytics/oia/idea/search/all/materializeddelta?pilot="+pilotforchart[0],
			title:"Datasets published",
			type:"Number",
			htmlid:"datasetPublished",
			pilottype:0,
			id:"kpi14"
		},
		kpi14header:{
			url:baseurl+"/analytics/ods/dataset/all/delta",
			title:"Datasets published",
			type:"Number",
			htmlid:"datasetPublished",
			pilottype:0,
			id:"kpi14"
		},	
		kpi11header:{
			url:baseurl+"/analytics/oia/needpublished/delta",
			title:"Needs in Implementation Phase",
			type:"Number",
			htmlid:"needNumber",
			pilottype:0,
			id:"kpi11"
		},

		kpi9header:{
			url:baseurl+"/analytics/oia/challengue/delta",
			title:"Challenge in Implementation Phase",
			type:"Number",
			htmlid:"challengePublished",
			pilottype:0,
			id:"kpi9"
		},
	
		kpi7header:{
			url:baseurl+"/analytics/oia/idea/delta",
			title:"Ideas in Implementation Phase",
			type:"Number",
			htmlid:"ideasPublished",
			pilottype:0,
			id:"kpi7"
		},

		kpi3:{
			url:baseurl+"/analytics/oia/ideainimplementation/"+ccUserId+"/count",
			title:"My ideas in Implementation Phase",
			type:"Pie",
			htmlid:"userIdeasImpl",
			pilottype:0,
			color:"#bebf03", 
			id:"kpi3"
		},	
		kpi6:{
			url:baseurl+"/analytics/oia/idea/search/"+ccUserId+"/materializedcity",
			title:"My Artefacts",
			type:"Bar2D",
			htmlid:"userArtefacts",
			pilottype:0,
			color:"#bebf03", 
			id:"kpi6"
		},		
		kpi8:{
			url:baseurl+"/analytics/oia/idea/"+ccUserId+"/published",
			title:"My Published Ideas",
			type:"Pie",
			htmlid:"userIdeas",
			pilottype:0,
			color:"#bebf03", 
			id:"kpi8"
		},	
		kpi10:{
			url:baseurl+"/analytics/oia/challengue/"+ccUserId+"/published",
			title:"My Published Challenges",
			type:"Pie",
			htmlid:"userChallenges",
			pilottype:0,
			color:"#bebf03", 
			id:"kpi10"
		},	
		kpi12:{
			url:baseurl+"/analytics/oia/needpublished/"+ccUserId+"/count",
			title:"My Published Needs",
			type:"Pie",
			htmlid:"userNeeds",
			pilottype:0,
			color:"#bebf03", 
			id:"kpi12"
		},	
		kpi18:{
			url:baseurl+"/analytics/ods/dataset/usage/"+ccUserId+"/count",
			title:"My datasets usage",
			type:"Pie",
			htmlid:"userDatasets",
			pilottype:0,
			color:"#e44240",
			id:"kpi18"
		},		
		kpi1header:{
			url:baseurl+"/analytics/cdv/user/all/count",
			title:"Registered Users",
			type:"Number",
			pilottype:0,
			htmlid:"registeredUsers",
			id:"kpi1"
		}
		
		
		
}


$( document ).ready(function() {
	
	document.cookie="language="+locale;
	llenarTarjetaNeeds(fakeNeedData, 'contenedorNeeds');
	llenarTarjetaIdeas(fakeIdeaData, 'contenedorIdeas');
	llenarTarjetaChallenges(fakeChallengeData, 'contenedorretos');
	//llenarTarjeta(fakeNeedData, 'contenedorNeeds');
	//llenarTarjeta(fakeIdeaData, 'contenedorIdeas');
	llenarTarjeta(fakeMarketplaceData, 'contenedorMarketplace');
	if (getUrlParameter('pilot')=='Bilbao'||getUrlParameter('pilot')=='Trento'||getUrlParameter('pilot')=='Uusimaa'||getUrlParameter('pilot')=='Novisad'){
		if (!isLoged) sessionStorage.pilot=getUrlParameter('pilot');


	}
	if (!istest){
		document.getElementById("aviso").style.display='none';
	}
	//if(sessionStorage.pilot!=null){
		document.getElementById("selector").style.display='none';
	//}
	document.getElementById('pilotArrow').style.left=($('#logo-container').position().left+190)+'px';
	if (role.indexOf("OnmiAdmin")>-1||role.indexOf("Developer")>-1||role.indexOf("Authority")>-1){
		console.log('User is a developer');
	}
	//else{
		//document.getElementById("botonTools").className+=" botongris";
	//}
	if (role.indexOf("OnmiAdmin")>-1){
		document.getElementById("titGeneral").innerHTML='<liferay-ui:message key="overlay.chart.titleAdmin" />';
		setHeaderNumberTotal(kpidata.kpi1header);
		setHeaderNumberTotal(kpidata.kpi7header);
		setHeaderNumberTotal(kpidata.kpi9header);
		setHeaderNumberTotal(kpidata.kpi11header);
		//setHeaderNumberTotal(kpidata.kpi14header);
		setHeaderNumberDATASET(kpidata.kpi14headerDATASETALL);	
		
	}
	else if (role.indexOf("Authority")>-1){
		document.getElementById("titGeneral").innerHTML="General data of "+pilotforchart[0]+" pilot";
		
		setHeaderNumber(kpidata.kpi1header);
		setHeaderNumber(kpidata.kpi7header);
		setHeaderNumber(kpidata.kpi9header);
		setHeaderNumber(kpidata.kpi11header);
		setHeaderNumberTotalDATASET(kpidata.kpi14headerDATASET);
		
		
	}
	if (isLoged && role.indexOf("OnmiAdmin")==-1 && role.indexOf("Authority")==-1){
		document.getElementById("titGeneral").innerHTML='<liferay-ui:message key="overlay.chart.titleCitizen" />'
	
		setUserHeaderNumber(kpidata.kpi3);
		setUserHeaderNumberArray(kpidata.kpi6);
		setUserHeaderNumber(kpidata.kpi8);
		setUserHeaderNumberArray(kpidata.kpi10);
		setUserHeaderNumber(kpidata.kpi12);
		setUserHeaderNumber(kpidata.kpi18);
	}
	if (!isLoged){
		llenarOpinion();
		//if (ccUserId >0 )setUserHeaderNumber(kpidata.kpi3);
	}
});

function getUrlParameter(name) { name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]'); var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'); var results = regex.exec(location.search); return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' ')); };
</script>

<style>

.portlet-body {
	padding: 0 !important;
	
	.navigation {
		background: transparent;
	}
}
.white{
	/*position:fixed;
	z-index:100;*/
}
#content{
	 padding: 0 !important;
}

</style>

<portlet:actionURL var="selectPilotURL" windowState="normal" name="selectPilot">
</portlet:actionURL>

<portlet:actionURL var="toolsURL" windowState="normal" name="tools">
</portlet:actionURL>

<portlet:actionURL var="selectToolURL" windowState="normal" name="selectTool">
</portlet:actionURL>

<liferay-theme:defineObjects />
<portlet:defineObjects />

	<div class=cuerpo>
		<i  style="display:none" onclick="ShowPilotMenu();" id="pilotArrow" class="material-icons right">arrow_drop_down</i>
		<div id ="divPilotSelect" class="divPilotSelect" onclick="this.style.display='none'">
			<div onclick="document.location='<%=selectPilotURL %>&amp;<portlet:namespace/>pilot=<%=URLEncoder.encode(Constants.PILOT_CITY_BILBAO, "UTF-8")%>'" class="pilotText">Bilbao</div>
			<div onclick="document.location='<%=selectPilotURL %>&amp;<portlet:namespace/>pilot=<%=URLEncoder.encode(Constants.PILOT_CITY_NOVISAD, "UTF-8")%>'" class="pilotText">Novi Sad</div>
			<div onclick="document.location='<%=selectPilotURL %>&amp;<portlet:namespace/>pilot=<%=URLEncoder.encode(Constants.PILOT_CITY_HELSINKI, "UTF-8")%>'" class="pilotText">Helsinki Region</div>
			<div onclick="document.location='<%=selectPilotURL %>&amp;<portlet:namespace/>pilot=<%=URLEncoder.encode(Constants.PILOT_CITY_TRENTO, "UTF-8")%>'" class="pilotText">Trento</div>
		</div>
		<%
		if(!themeDisplay.isSignedIn() || user == null) {	
		%>
		<div class="bloque1">
			<div class="margenTitulo">
				<div class="titulo textoTitulo centrado"><liferay-ui:message key="overlay.block1.title" /> </div>
				<div class=titulo>
					<div class="textoDelgado centrado"><liferay-ui:message key="overlay.block1.title2" /></div>		
					<div class="textoDelgado centrado"><liferay-ui:message key="overlay.block1.title3" /></div>	
				</div>	
				<div class="titulo textoPortada centrado"><liferay-ui:message key="overlay.block1.title4" /></div>		
				<%
			    if(!themeDisplay.isSignedIn() || user == null) {	
				%>
				<div class="botonTitulo boton" onclick="goToRegister('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_AAC, "UTF-8")%>')"><liferay-ui:message key="overlay.block1.button" /></div>
				<% }%>
			</div>
				<img onclick="scroolTo('bloque2')" src="<%=imgPath%>/controller/downwhite.png" class="arrowDown">
				
		</div>
		
		<!-- Bloque iconitos-->
			<div id="bloque2" class="bloque2">
				<div class="titulo textoTitulo centrado"><liferay-ui:message key="overlay.block2.title" /></div>
				<div class="bloqueCentrado">
					<div class="div25">
						<div class="margenDiv25">
							<div class="iconRedondoGr iconphone"></div>
							<div class="titDelgado"><liferay-ui:message key="overlay.block2.title1" /></div>
							<div class="textoNormal"><liferay-ui:message key="overlay.block2.title2" /></div>
						</div>
					</div>
					<div class="div25">
						<div class="margenDiv25">
							<div class="iconRedondoGr iconidea"></div>
							<div class="titDelgado"><liferay-ui:message key="overlay.block2.title3" /></div>
							<div class="textoNormal"><liferay-ui:message key="overlay.block2.title4" /></div>
						</div>
					</div>
					<div class="div25">
						<div class="margenDiv25">
							<div class="iconRedondoGr iconheart"></div>
							<div class="titDelgado"><liferay-ui:message key="overlay.block2.title5" /></div>
							<div class="textoNormal"><liferay-ui:message key="overlay.block2.title6" /></div>
						</div>
					</div>
					<div class="div25">
						<div class="margenDiv25">
							<div class="iconRedondoGr iconllave"></div>
							<div class="titDelgado"><liferay-ui:message key="overlay.block2.title7" /></div>
							<div class="textoNormal"><liferay-ui:message key="overlay.block2.title8" /></div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- Bloque de opinion, enlace al  OIA-->
			<div id="bloque3" class="bloqueNormal">
				<div class="bloqueCentrado">
					<div class="div50">
						<div class="izquierda seccion textoTitulo"><liferay-ui:message key="overlay.block3.title" />&nbsp; <%=pilot%> </div>
						<div class="izquierda seccion textoNormal"><liferay-ui:message key="overlay.block3.title1" /></div>
						<div style="background-image: url('<%=imgPath%>/controller/icon-idea.png');" class="izquierda seccion iconoboton boton" onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_OIA, "UTF-8")%>')"><liferay-ui:message key="overlay.block3.button" /></div>
					
						<div id="cityopinion">
						</div>		
					</div>
					<div class="div50">
						<div class="imagen50" style="background-image: url('<%=imgPath%>/controller/imagen-ciudad.jpeg');"></div>
					</div>
				</div>
			</div>
		
						
			<%
			}
			else if(isAuthority || role.contains("OnmiAdmin")){
			%>
			<!-- Si es authoryti algunas estadisticas -->
			<div id="bloque3" class="bloqueNormal">
				<div class="bloqueCentrado">
					
					<div id="titGeneral"class="seccion textoTitulo centrado"> </div>
					<div class="chartContainer">
						<!-- <div class="chartCabecera">
							<span id="titGeneral" >General data</span>
						</div> -->
						<div id="contkpi0" class="chartBody">
							<div style="float:left;width:20%"  class="tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="overlay.block3.users"/>'>
								<div class="contenedorValue">
									<div class="cabeceraValue"><liferay-ui:message key="overlay.block3.users" /></div>
									<div id="registeredUsers" class="valorValue">0</div>
									
								</div>
							</div>
							<a href="/ideas_explorer?my=true">
							<div style="float:left;width:20%" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="overlay.block3.ideas"/>'>
								<div class="contenedorValue">
									<div class="cabeceraValue"><liferay-ui:message key="overlay.block3.ideas" /></div>
									<div id="ideasPublished" class="valorValue">0</div>
									
								</div>
							</div>
							</a>
							<div style="float:left;width:20%" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="overlay.block3.challenges"/>'>
								<div class="contenedorValue">
									<div class="cabeceraValue"><liferay-ui:message key="overlay.block3.challenges" /></div>
									<div id="challengePublished" class="valorValue">0</div>
									
								</div>
							</div>
							
							<a href="/needs_explorer?my=true">
							<div style="float:left;width:20%" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="overlay.block3.needs"/>'>
								<div class="contenedorValue">
									<div class="cabeceraValue"><liferay-ui:message key="overlay.block3.needs" /></div>
									<div id="needNumber" class="valorValue">0</div>
									
								</div>
							</div>
							</a>
							<div style="float:left;width:20%" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="overlay.block3.datasets"/>'>
								<div class="contenedorValue">
									<div class="cabeceraValue"><liferay-ui:message key="overlay.block3.datasets" /></div>
									<div id="datasetPublished" class="valorValue">0</div>
									
								</div>
							</div>
												
						</div>	
					</div>				
					<br/>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block3.title1Authority" /></div>
					<div style="background-image: url('<%=imgPath%>/controller/icon_pie_chart_white_24dp_2x.png');" class="centrado iconoboton boton" onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_ADS, "UTF-8")%>')"><liferay-ui:message key="overlay.block3.buttonAuthority" /></div>
					
					
					
				</div>
			</div>			
			<% }else{%>
			<!-- Logged user -->
			<div id="bloque3" class="bloqueNormal">
				<div class="bloqueCentrado">
					
					<div id="titGeneral" class="seccion textoTitulo centrado"> </div>
					<div class="chartContainer">
						<!-- <div class="chartCabecera">
							
						</div>-->
						<div id="contkpi0" class="chartBody">
							<a href="/needs_explorer?my=true">
							<div style="float:left;width:16.5%"  class="tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="overlay.block3.myneeds"/>'>
								<div class="contenedorValue">
									<div class="cabeceraValue"><liferay-ui:message key="overlay.block3.myneeds" /></div>
									<div id="userNeeds" class="valorValue">0</div>
									
								</div>
							</div>	
							</a>
							<div style="float:left;width:16.5%" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="overlay.block3.mychallenges"/>'>
								<div class="contenedorValue">
									<div class="cabeceraValue"><liferay-ui:message key="overlay.block3.mychallenges" /></div>
									<div id="userChallenges" class="valorValue">0</div>
									
								</div>
							</div>
							<a href="/ideas_explorer?my=true">
							<div style="float:left;width:16.5%" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="overlay.block3.myideas"/>'>
								<div class="contenedorValue">
									<div class="cabeceraValue"><liferay-ui:message key="overlay.block3.myideas" /></div>
									<div id="userIdeas" class="valorValue">0</div>
									
								</div>
							</div>
							</a>		
							<a href="/ideas_explorer?my=true">					
							<div style="float:left;width:16.5%" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="overlay.block3.myideasimp"/>'>
								<div class="contenedorValue">
									<div class="cabeceraValue"><liferay-ui:message key="overlay.block3.myideasimp" /></div>
									<div id="userIdeasImpl" class="valorValue">0</div>
									<!-- <div class="unidadesValue"></div> -->
								</div>
							</div>
							</a>
							<div style="float:left;width:16.5%" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="overlay.block3.myartefacts"/>'>
								<div class="contenedorValue">
									<div class="cabeceraValue"><liferay-ui:message key="overlay.block3.myartefacts" /></div>
									<div id="userArtefacts" class="valorValue">0</div>
									
								</div>
							</div>
							<div style="float:left;width:16.5%" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="overlay.block3.datasetusage"/>'>
								<div class="contenedorValue">
									<div class="cabeceraValue"><liferay-ui:message key="overlay.block3.datasetusage" /></div>
									<div id="userDatasets" class="valorValue">0</div>
									
								</div>
							</div>												
						</div>	
					</div>				
					<br/>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block3.title1Authority" /></div>
					<div style="background-image: url('<%=imgPath%>/controller/icon_pie_chart_white_24dp_2x.png');" class="centrado iconoboton boton" onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_ADS, "UTF-8")%>')"><liferay-ui:message key="overlay.block3.buttonAuthority" /></div>
					
					
					
				</div>
			</div>						
			<% }%>
			<!-- Bloque needs enlace a  all needs-->
			<div id="bloqueneeds" class="bloqueNormal">
				<div class="margenCentrado">
				<%if (!userHasNeeds){%>					
					<div class="seccion textoTitulo centrado"><liferay-ui:message key="overlay.block4.title" /></div>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block4.title1" /></div>
				<%}else if (!pilotHasNeeds){%>
					<div class="seccion textoTitulo centrado"><liferay-ui:message key="overlay.block4.titleAlt2" /></div>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block4.title1Alt2" /></div>
				<%}else if (!langHasNeeds){%>
					<div class="seccion textoTitulo centrado"><liferay-ui:message key="overlay.block4.titleAlt3" /></div>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block4.title1Alt3" /></div>				
				<%}else{%>
					<div class="seccion textoTitulo centrado"><liferay-ui:message key="overlay.block4.titleAlt1" /></div>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block4.title1Alt1" /></div>
					
				<%}%>	
				<div  class="bloqueCentrado">	
				<%
			    if(!themeDisplay.isSignedIn() || user == null ) {	
				%>
				<div class="div33">
					<div style="background-image: url('<%=imgPath%>/controller/icon-need.png');" class="centrado iconoboton boton"  onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_NEEDS, "UTF-8")%>')"><liferay-ui:message key="overlay.block4.button" /></div>
				</div>	
				<div class="div33">				
					<div style="background-image: url('<%=imgPath%>/controller/icon-create.png');" class="centrado iconoboton boton disabled tooltipped" data-position="bottom" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.block4.newneed.tooltip.button" />"> <liferay-ui:message key="overlay.block4.newneed.button" /></div>
				</div>
				<%
			    }else if (isSimpleUser){
				%>
				<div class="div33">
					<div style="background-image: url('<%=imgPath%>/controller/icon-need.png');" class="centrado iconoboton boton"  onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_NEEDS, "UTF-8")%>')"><liferay-ui:message key="overlay.block4.button" /></div>
				</div>	
				<div class="div33">					
					<div style="background-image: url('<%=imgPath%>/controller/icon-create.png');" class="centrado iconoboton boton"  onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_NEEDS_NEW, "UTF-8")%>')"><liferay-ui:message key="overlay.block4.newneed.button" /></div>
				</div>
				
			   <% }else{%>
				<div class="div33">
					<div style="background-image: url('<%=imgPath%>/controller/icon-need.png');" class="centrado iconoboton boton"  onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_NEEDS, "UTF-8")%>')"><liferay-ui:message key="overlay.block4.button" /></div>
				</div>			    	
			   <% 
			   }
				%>
				</div>
				</div>
				
				<div id="contenedorNeeds" class="bloqueCentrado">
				</div>
			</div>
			<div id="bloqueretos" class="bloqueNormal">
				<div class="margenCentrado">
								
					<div class="seccion textoTitulo centrado"><liferay-ui:message key="overlay.blockretos.title" /></div>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.blockretos.title1" /></div>
					

				<div  class="bloqueCentrado">	
				<div class="div33">
					<div style="background-image: url('<%=imgPath%>/controller/icons-challenge.png');" class="centrado iconoboton boton"  onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_CHALLENGES, "UTF-8")%>')"><liferay-ui:message key="overlay.blockretos.button" /></div>
				</div>	
				
				<%
			    if(isAuthority) {				
				%>
				<div class="div33">
				<div style="background-image: url('<%=imgPath%>/controller/icon-create.png');" class="centrado iconoboton boton"  onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_CHALLENGE_NEW, "UTF-8")%>')"><liferay-ui:message key="overlay.blockretos.newchallenge.button" /></div>
				</div>
				<%
			    }
				%>
					
				</div>							
				</div>
				<div id="contenedorretos" class="bloqueCentrado">
				</div>
			</div>			
			<div id="bloqueideas" class="bloqueNormal">
				<div class="margenCentrado">
				<%if (!userHasIdeas){%>
					<div class="seccion textoTitulo centrado"><liferay-ui:message key="overlay.block5.title" /></div>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block5.title1" /></div>
				<%}else if (!pilotHasIdeas){%>
					<div class="seccion textoTitulo centrado"><liferay-ui:message key="overlay.block5.titleAlt2" /></div>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block5.title1Alt2" /></div>
				<%}else if (!langHasIdeas){%>
					<div class="seccion textoTitulo centrado"><liferay-ui:message key="overlay.block5.titleAlt3" /></div>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block5.title1Alt3" /></div>				
					
				<%}else{%>
					<div class="seccion textoTitulo centrado"><liferay-ui:message key="overlay.block5.titleAlt1" /></div>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block5.title1Alt1" /></div>
				
				<%}%>
				
				<div  class="bloqueCentrado">	
				<%
			    if(!themeDisplay.isSignedIn() || user == null ) {	
				%>
				<div class="div33">
					<div style="background-image: url('<%=imgPath%>/controller/icon-idea.png');" class="centrado iconoboton boton"  onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_IDEAS, "UTF-8")%>')"><liferay-ui:message key="overlay.block5.button" /></div>
				</div>	
				<div class="div33">
				
					<div style="background-image: url('<%=imgPath%>/controller/icon-create.png');" class="centrado iconoboton boton disabled tooltipped" data-position="bottom" data-delay="50" data-tooltip="<liferay-ui:message key="overlay.block5.newidea.tooltip.button" />"> <liferay-ui:message key="overlay.block5.newidea.button" /></div>
				</div>
				<%
			    }else if(isSimpleUser){
				%>
				<div class="div33">
					<div style="background-image: url('<%=imgPath%>/controller/icon-idea.png');" class="centrado iconoboton boton"  onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_IDEAS, "UTF-8")%>')"><liferay-ui:message key="overlay.block5.button" /></div>
				</div>	
				<div class="div33">
				
				<div style="background-image: url('<%=imgPath%>/controller/icon-create.png');" class="centrado iconoboton boton"  onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_IDEAS_NEW, "UTF-8")%>')"><liferay-ui:message key="overlay.block5.newidea.button" /></div>
				</div>
				<%
			    } else{
			    %>
				<div class="div33">
					<div style="background-image: url('<%=imgPath%>/controller/icon-idea.png');" class="centrado iconoboton boton"  onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_IDEAS, "UTF-8")%>')"><liferay-ui:message key="overlay.block5.button" /></div>
				</div>	
			    	
			    <%
			    }
				%>
				</div>
				</div>
				<div id="contenedorIdeas" class="bloqueCentrado">
				
				</div>
			</div>
			<div id="bloque4" class="bloqueNormal">
				<div class="bloqueCentrado">
					<div class="div50">
						<div class="imagen50" style="background-image: url('<%=imgPath%>/controller/imagen-movil.jpeg');"></div>
					</div>
					<div class="div50">
						<div class="izquierda seccion textoTitulo"><liferay-ui:message key="overlay.block6.title" /></div>
						<div class="izquierda seccion textoNormal"><liferay-ui:message key="overlay.block6.title1" /></div>
						<div id="botonTools" style="background-image: url('<%=imgPath%>/controller/icon-llave.png');" class="izquierda seccion iconoboton boton"  onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_VC, "UTF-8")%>');"><liferay-ui:message key="overlay.block6.button" /></div>
						<div class="izquierda seccion textoCursiva">"WeLive tools are designed with a user-centered perspective in mind We wanted to  create easy to use features, so that you don't need high level engineering skills to build an app. With WeLive tools anyone can be a designer"</div>
						<div class="izquierda seccion textoNormal textoPerfil"><img class="fotoPerfil"  src="<%=imgPath%>/controller/photo.jpg">Zurik Corera. Tecnalia Research and Innovation</div>
					</div>
				</div>
			</div>
			<div id="bloque5" class="bloqueNormal">
				<div class="margenCentrado">
				<%if (!userHasArtefacts){%>
					<div class="seccion textoTitulo centrado"><liferay-ui:message key="overlay.block7.title" /></div>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block7.title1" /></div>
				<%}else if (!pilotHasArtefacts){%>
					<div class="seccion textoTitulo centrado"><liferay-ui:message key="overlay.block7.titleAlt2" /></div>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block7.title1Alt2" /></div>
				<%}else if (!langHasArtefacts){%>
					<div class="seccion textoTitulo centrado"><liferay-ui:message key="overlay.block7.titleAlt3" /></div>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block7.title1Alt3" /></div>				
					
				<%}else{%>	
					<div class="seccion textoTitulo centrado"><liferay-ui:message key="overlay.block7.titleAlt1" /></div>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block7.title1Alt1" /></div>			
				<%}%>				
					<div style="background-image: url('<%=imgPath%>/controller/icon-cart.png');" class="centrado iconoboton boton"  onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_MKP, "UTF-8")%>')"><liferay-ui:message key="overlay.block7.button" /></div>
				</div>
				<div id="contenedorMarketplace" class="bloqueCentrado">
				</div>
			</div>
			<div id="bloque10" class="bloqueNormal">
				<div class="margenCentrado">
					<div class="seccion textoTitulo centrado"><liferay-ui:message key="overlay.block10.title" /></div>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block10.title1" /> </div>
					<div style="background-image: url('<%=imgPath%>/controller/icons-database.png');" class="centrado iconoboton boton"  onclick="selectPage('<%=selectToolURL %>&amp;<portlet:namespace/>tool=<%=URLEncoder.encode(Constants.TOOL_ODS, "UTF-8")%>')"><liferay-ui:message key="overlay.block10.button" /></div>
				</div>
			</div>				
			<div id="bloque6" class="bloqueNormal">
				<div class="margenCentrado">
					<div class="seccion textoTitulo centrado"><liferay-ui:message key="overlay.block8.title" /></div>
					<div class="seccion centrado textoNormal"><liferay-ui:message key="overlay.block8.title1" /> </div>
					<img onclick="window.open('https://play.google.com/store/apps/details?id=it.smartcommunitylab.weliveplayer');" class="botonDownload centrado"src="<%=imgPath%>/controller/googleplay.png">
				</div>
			</div>

		<div class="bloquefin"></div>
		</div>
		<div id="selector" class="selectorPilot section no-pad">
			<div class="popupSelector">
				<img class="iconWelive centrado"src="<%=imgPath%>/controller/welive.png">
				<div class="titulo textoNormal"><liferay-ui:message key="overlay.block9.title" /></div>
				<div class="bloqueCentrado">
				<!-- 
					<div class="botonPilot" onclick="document.location='<%=selectPilotURL %>&amp;<portlet:namespace/>pilot=<%=URLEncoder.encode(Constants.PILOT_CITY_BILBAO, "UTF-8")%>'"><img class="fotoPilot" src="<%=imgPath%>/controller/Bilbao.jpg"><div class="textoNormal">Bilbao</div></div>
					<div class="botonPilot" onclick="document.location='<%=selectPilotURL %>&amp;<portlet:namespace/>pilot=<%=URLEncoder.encode(Constants.PILOT_CITY_HELSINKI, "UTF-8")%>'"><img class="fotoPilot" src="<%=imgPath%>/controller/Uusimaa.jpg"><div class="textoNormal">Helsinki Region</div></div>
					<div class="botonPilot" onclick="document.location='<%=selectPilotURL %>&amp;<portlet:namespace/>pilot=<%=URLEncoder.encode(Constants.PILOT_CITY_NOVISAD, "UTF-8")%>'"><img class="fotoPilot" src="<%=imgPath%>/controller/Novisad.jpg"><div class="textoNormal">Novi Sad</div></div>
					<div class="botonPilot" onclick="document.location='<%=selectPilotURL %>&amp;<portlet:namespace/>pilot=<%=URLEncoder.encode(Constants.PILOT_CITY_TRENTO, "UTF-8")%>'"><img class="fotoPilot" src="<%=imgPath%>/controller/Trento.jpg"><div class="textoNormal">Trento</div></div>
					-->
					<div class="botonPilot" onclick="changePage('<%=URLEncoder.encode(Constants.PILOT_CITY_BILBAO, "UTF-8")%>')"><img class="fotoPilot" src="<%=imgPath%>/controller/Bilbao.jpg"><div class="textoNormal">Bilbao</div></div>
					<div class="botonPilot" onclick="changePage('<%=URLEncoder.encode(Constants.PILOT_CITY_HELSINKI, "UTF-8")%>')"><img class="fotoPilot" src="<%=imgPath%>/controller/Uusimaa.jpg"><div class="textoNormal">Helsinki Region</div></div>
					<div class="botonPilot" onclick="changePage('<%=URLEncoder.encode(Constants.PILOT_CITY_NOVISAD, "UTF-8")%>')"><img class="fotoPilot" src="<%=imgPath%>/controller/Novisad.jpg"><div class="textoNormal">Novi Sad</div></div>
					<div class="botonPilot" onclick="changePage('<%=URLEncoder.encode(Constants.PILOT_CITY_TRENTO, "UTF-8")%>')"><img class="fotoPilot" src="<%=imgPath%>/controller/Trento.jpg"><div class="textoNormal">Trento</div></div>
					
				</div>
				<!-- <div class="titulo textoCursiva"><liferay-ui:message key="overlay.block9.title1" /></div> -->
			</div>
		</div>
       <div id="aviso" class="fijo">
       <p style=""> <liferay-ui:message key="overlay.advise" /></p>
       <p><liferay-ui:message key="overlay.advise1" />&nbsp; <a target="_blank" href="http://dev.welive.eu" >http://dev.welive.eu</a></p>
        
       </div>						


<%@ include file="/html/essential-overlay/appendix_script.jspf" %>
