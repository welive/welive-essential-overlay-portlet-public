	function setHeaderNumber(kpi){
	    $.ajax({
	        url: kpi.url,
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	var result=jdata[pilotforchart[kpi.pilottype]];
	        	if (!result){
	        		result=0;
	        	}
	        	document.getElementById(kpi.htmlid).innerHTML=result;
	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create1DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}
	function setHeaderNumberTotal(kpi){
	    $.ajax({
	        url: kpi.url,
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	var labels=Object.keys(jdata);
	        	var result=0;
	        	for (var i=0; i<labels.length;i++){
	        		result+=jdata[labels[i]];
	        	}
	        	document.getElementById(kpi.htmlid).innerHTML=result;
	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al setHeaderNumberTotal!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}	
	function setHeaderNumberDATASET(kpi){
		$.ajax({
	        url: kpi.url,
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	        	var result = 0;   
	        	for (var i =0; i<jdata.datasets.length;i++){
	        		
	        		if (jdata.datasets[i].label == "Dataset"){
	        			var values = jdata.datasets[i].values;
	        			console.log(values);
	        			for (var v=0;v<values.length;v++){
	    	        		
	    		            result =result+ values[v];
	        			}
	    		   
	        		}
	        	}
	        	document.getElementById(kpi.htmlid).innerHTML=result;

	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al setHeaderNumberDATASET!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });			
	}
	function setHeaderNumberTotalDATASET(kpi){
	    $.ajax({
	        url: kpi.url,
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	        	
	        	var labels=Object.keys(jdata)
	        	var data=getObjValues(jdata, labels);
	        	if (labels.length!=0){
			        for (var i =0; i<labels.length;i++){
			        	if (labels[i]=="Dataset"){
			        		document.getElementById(kpi.htmlid).innerHTML=data[i];		        		
			        	}
		        	}
		        }else{
		        	document.getElementById(kpi.htmlid).innerHTML="0";
				}

	        	
	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al setHeaderNumberTotalDATASET!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}		
	function getObjValues(jdata, arr){
		var retorno=new Array();
		for (var i=0;i<arr.length;i++){
			retorno.push(jdata[arr[i]]);
		}
		return retorno;
	}
	function setUserHeaderNumber(kpi){
	    $.ajax({
	        url: kpi.url,
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	        	var labels=Object.keys(jdata);
	        	var chartdata= new Array();
	        	var result=0;
	        	for (var i=0;i<labels.length;i++){
	        		result = result + jdata[labels[i]];
	        		//chartdata[i]={
	                  //      value: jdata[labels[i]],
	                        //color:"rgba("+colores[i]+",0.5)",
	                        //highlight: "rgba("+colores[i]+",0.75)",
	                    //    label: labels[i]
	                 //}
		        	if (!result){
		        		result=0;
		        	}
	        		
	        		document.getElementById(kpi.htmlid).innerHTML=result;
	        	}
	        	console.log(chartdata);
	        	//createChart(kpi.id, "Pie", msgLabels[kpi.id], chartdata, "", kpi.color);

	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create1DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}	
	function setUserHeaderNumberArray(kpi){
	    $.ajax({
	        url: kpi.url,
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	        	var labels=Object.keys(jdata);
	        	var chartdata= new Array();
	        	var result=0;
	        	for (var i=0;i<labels.length;i++){
	        		if (labels[i] == "datasets" ){
	          			var arryobj=jdata[labels[i]];
	        			for (var j=0;j<arryobj.length;j++){
	        				var obj = arryobj[j];
	        				var values = obj.values;
	        				for (var k=0;k<values.length;k++){
	        					result = result + values[k];
	        				}
	        			}
	        			
	        			
	        		}
	        		else if (labels[i] == "IdeaPublished"){
	        			result = result + jdata[labels[i]];
	        		}
	        		//result = result + jdata[labels[i]];
	        		//chartdata[i]={
	                  //      value: jdata[labels[i]],
	                        //color:"rgba("+colores[i]+",0.5)",
	                        //highlight: "rgba("+colores[i]+",0.75)",
	                    //    label: labels[i]
	                 //}
	        		

	        	}
	        	if (!result){
	        		result=0;
	        	}
	        	
        		document.getElementById(kpi.htmlid).innerHTML=result;
	        	
	        	console.log(chartdata);
	        	//createChart(kpi.id, "Pie", msgLabels[kpi.id], chartdata, "", kpi.color);

	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create1DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}	