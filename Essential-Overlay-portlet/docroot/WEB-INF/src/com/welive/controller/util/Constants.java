package com.welive.controller.util;

public final class Constants {
	public final static String YES = "yes";
	public final static String NO = "no";
	
	public final static String TRUE = "true";
	public final static String FALSE = "false";
	
	public final static String PILOT_CITY_TRENTO = "Trento";
	public final static String PILOT_CITY_BILBAO = "Bilbao";
	public final static String PILOT_CITY_NOVISAD = "Novisad";
	public final static String PILOT_CITY_HELSINKI = "Uusimaa";
	public final static String PILOT_CITY_LIST[] = {PILOT_CITY_TRENTO, PILOT_CITY_BILBAO, PILOT_CITY_NOVISAD, PILOT_CITY_HELSINKI};
	
	public final static String ROLE_CITIZEN = "Citizen";
	public final static String ROLE_AUTHORITY = "Authority";
	public final static String ROLE_COMPANY_LEADER = "CompanyLeader";
	public final static String ROLE_ACADEMY = "Academy";
	public final static String ROLE_BUSINESS = "Business";
	public final static String ROLE_ENTREPRENEURE = "Entrepreneur";
	public final static String ROLE_DEVELOPER = "Developer";
	public final static String ROLE_ADMIN = "OnmiAdmin";
	public final static String ROLE_LIST[] = {ROLE_CITIZEN,ROLE_AUTHORITY ,ROLE_COMPANY_LEADER,ROLE_ACADEMY, ROLE_BUSINESS, ROLE_ENTREPRENEURE,ROLE_DEVELOPER,ROLE_ADMIN};

	public final static String EMPLOYMENT_STUDENT = "Student";
	public final static String EMPLOYMENT_UNEMPLOYED = "Unemployed";
	public final static String EMPLOYMENT_3RD = "Employedbythirdparty";
	public final static String EMPLOYMENT_SELF = "Selfemployedentrepreneur";
	public final static String EMPLOYMENT_RETIRED = "Retired";
	public final static String EMPLOYMENT_OTHER = "Other";
	public final static String EMPLOYMENT_LIST[] = {EMPLOYMENT_STUDENT, EMPLOYMENT_UNEMPLOYED, EMPLOYMENT_3RD, EMPLOYMENT_SELF, EMPLOYMENT_RETIRED, EMPLOYMENT_OTHER};
	
	public final static String LANGUAGE_ENGLISH = "English";
	public final static String LANGUAGE_SPANISH = "Spanish";
	public final static String LANGUAGE_ITALIAN = "Italian";
	public final static String LANGUAGE_FINNISH = "Finnish";
	public final static String LANGUAGE_SERBIAN = "Serbian";
	public final static String LANGUAGE_SERBIAN_LATIN = "SerbianLatin";
	public final static String LANGUAGE_LIST[] = {LANGUAGE_ENGLISH, LANGUAGE_SPANISH, LANGUAGE_ITALIAN, LANGUAGE_FINNISH, LANGUAGE_SERBIAN, LANGUAGE_SERBIAN_LATIN};

	public final static String TOOL_WC = "Controller";
	public final static String TOOL_OIA = "Open Innovation Area";
	public final static String TOOL_ODS = "Open Data Stack";
	public final static String TOOL_CDV = "Citizen Data Vault";
	public final static String TOOL_VC = "Visual Composer";
	public final static String TOOL_MKP = "Marketplace";
	public final static String TOOL_ADS = "Analytics Dashboard";
	public final static String TOOL_NEEDS = "Needs Explorer";
	public final static String TOOL_NEEDS_NEW = "New Need";
	public final static String TOOL_IDEAS = "Ideas Explorer";
	public final static String TOOL_IDEAS_NEW = "New Idea";
	public final static String TOOL_CHALLENGES = "Challenges Explorer";
	public final static String TOOL_CHALLENGE_NEW = "New Challenge";
	public final static String TOOL_AAC = "Logging Page";
	public final static String TOOL_LIST[] = {TOOL_OIA, TOOL_ODS, TOOL_CDV, TOOL_VC, TOOL_MKP, TOOL_ADS};
}
