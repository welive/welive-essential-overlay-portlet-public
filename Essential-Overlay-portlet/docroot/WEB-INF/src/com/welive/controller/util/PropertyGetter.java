package com.welive.controller.util;

import java.util.ResourceBundle;

public class PropertyGetter {
	private static final String BUNDLE_NAME = "config";

	private ResourceBundle bundle;

	private static PropertyGetter instance;

	private PropertyGetter() {
		bundle = ResourceBundle.getBundle(BUNDLE_NAME);
	}

	public static PropertyGetter getInstance() {
		if (instance == null) {
			instance = new PropertyGetter();
		}
		return instance;
	}

	public String getProperty(String key) {
		return bundle.getString(key);
	}
}
