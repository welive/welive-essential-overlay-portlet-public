package com.welive.controller.portlet;

import java.io.IOException;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.welive.controller.logging.Logger;
import com.welive.controller.util.Constants;
import com.welive.controller.util.PropertyGetter;

public class ControllerPortlet extends MVCPortlet {
	public static final String KEY_PILOT = "LIFERAY_SHARED_pilot";
	
	private static final Log _log = LogFactoryUtil.getLog(ControllerPortlet.class.getName());

	@Override
	public void init() throws PortletException {
		super.init();
	}

	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		PortletSession session = renderRequest.getPortletSession();
		
		//session.removeAttribute(KEY_PILOT, PortletSession.APPLICATION_SCOPE);
		/*String pilot = ParamUtil.getString(renderRequest, KEY_PILOT, null);
		if (pilot!= null){
			session.setAttribute(KEY_PILOT, pilot, PortletSession.APPLICATION_SCOPE);
			Logger.getInstance().postCitySelected(pilot);
		}*/
		super.doView(renderRequest, renderResponse);
		
//		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
//		if(themeDisplay.isSignedIn()) {
//			viewTemplate = "/html/essential-overlay/tools.jsp";
//		} else {
//			viewTemplate = "/html/essential-overlay/view.jsp";
//		}
	}
	
	public void tools(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		PortletSession session = actionRequest.getPortletSession();
		//session.removeAttribute(KEY_PILOT, PortletSession.APPLICATION_SCOPE);
		
		actionResponse.setRenderParameter("mvcPath", "/html/essential-overlay/tools.jsp");
	}
	
	public void selectPilot(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		String pilot = ParamUtil.getString(actionRequest, "pilot", null);

		PortletSession session = actionRequest.getPortletSession();
		if(pilot == null) {
			session.removeAttribute(KEY_PILOT, PortletSession.APPLICATION_SCOPE);
			return;
		}
		boolean result = Logger.getInstance().postCitySelected(pilot);
		session.setAttribute(KEY_PILOT, pilot, PortletSession.APPLICATION_SCOPE);
		//actionResponse.sendRedirect("/web/welive/ads");
		/*Cambiado por zurik*/
		//actionResponse.setRenderParameter("mvcPath", "/html/essential-overlay/tools.jsp");
		actionResponse.setRenderParameter("mvcPath", "/html/essential-overlay/view.jsp");
	}
	
	public void selectTool(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		boolean result;
		/*Cambiado por zurik*/
		String pilot = ParamUtil.getString(actionRequest, "pilot", null);
		String tool = ParamUtil.getString(actionRequest, "tool", null);
		PortletSession session = actionRequest.getPortletSession();
		if(pilot == null && !Constants.TOOL_AAC.equals(tool)) {
			session.removeAttribute(KEY_PILOT, PortletSession.APPLICATION_SCOPE);
			return;
		}
		if (pilot != null) {
			result = Logger.getInstance().postCitySelected(pilot);
			session.setAttribute(KEY_PILOT, pilot, PortletSession.APPLICATION_SCOPE);
		}
		else{
			session.removeAttribute(KEY_PILOT, PortletSession.APPLICATION_SCOPE);
		}
		/*Fin del cambio de Zurik*/
		
		Locale locale = actionRequest.getLocale();
		String sLanguage = locale.getLanguage();
		
		boolean logged = false;
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		User user = themeDisplay.getUser();
		if(themeDisplay.isSignedIn() && user != null) {
			logged = true;
		}
		
		result = Logger.getInstance().postComponentSelected(tool);
		if(Constants.TOOL_OIA.equals(tool)) {
			actionResponse.sendRedirect("/innovation-area");
		} else if(Constants.TOOL_ODS.equals(tool)) {
			String sUrl = "/ods";
			//if (sLanguage.equals(new Locale("sr").getLanguage())) {
			//	sUrl = String.format("%s/%s", sUrl, "sr_Latn");
			//} else {
			sUrl = String.format("%s/%s", sUrl, sLanguage);
			//}
			String odspilot="organization";
			if (pilot.equals(Constants.PILOT_CITY_BILBAO)){
				odspilot = String.format("%s/%s", odspilot, "bilbao-city-council");				
			}
			else if (pilot.equals(Constants.PILOT_CITY_HELSINKI)){
				odspilot = String.format("%s/%s", odspilot, "helsinki-uusimaa");	
				
			}
			else if (pilot.equals(Constants.PILOT_CITY_NOVISAD)){
				odspilot = String.format("%s/%s", odspilot, "novi-sad");	
				
			}
			else if (pilot.equals(Constants.PILOT_CITY_TRENTO)){
				odspilot = String.format("%s/%s", odspilot, "trento");	
				
			}
			sUrl = String.format("%s/%s", sUrl, odspilot);
			
			if(logged) {
				sUrl = String.format("%s?%s", sUrl, "login=true");
			}
			actionResponse.sendRedirect(sUrl);
		} else if(Constants.TOOL_CDV.equals(tool)) {
			String sUrl = "/cdv";
			if ("sr_RS_latin".equals(locale.toString())) {
				sUrl = String.format("%s/%s", sUrl, "sr-latin");
			} else {
				sUrl = String.format("%s/%s", sUrl, sLanguage);
			}
			sUrl = String.format("%s/%s", sUrl, "user");
			if(logged) {
				sUrl = String.format("%s?%s", sUrl, "login=true");
			}
			actionResponse.sendRedirect(sUrl);
		} else if(Constants.TOOL_VC.equals(tool)) {
			actionResponse.sendRedirect("/visualcomposer/user/home");
		} else if(Constants.TOOL_MKP.equals(tool)) {
			actionResponse.sendRedirect("/marketplace");
		} else if(Constants.TOOL_ADS.equals(tool)) {
			actionResponse.sendRedirect("/ads");
		} else if(Constants.TOOL_NEEDS.equals(tool)) {
			actionResponse.sendRedirect("/needs_explorer");
		} else if(Constants.TOOL_NEEDS_NEW.equals(tool)) {
			actionResponse.sendRedirect("/innovation-area#sectionOne/slide1");
		} else if(Constants.TOOL_IDEAS.equals(tool)) {
			actionResponse.sendRedirect("/ideas_explorer");
		} else if(Constants.TOOL_IDEAS_NEW.equals(tool)) {
			actionResponse.sendRedirect("/innovation-area#sectionOne/slide3");
		} else if(Constants.TOOL_CHALLENGES.equals(tool)) {
			actionResponse.sendRedirect("/challenges_explorer");
		} else if(Constants.TOOL_CHALLENGE_NEW.equals(tool)) {
			actionResponse.sendRedirect("/challenges_explorer/-/challenges_explorer_contest/new-challenge");
		} else if(Constants.TOOL_AAC.equals(tool)) {
			String urllenguage="";
			if (sLanguage.equals("es")){
				urllenguage = "es_ES";				
			}
			else if (sLanguage.equals("en")){
				urllenguage = "en_GB";				
			}
			else if (sLanguage.equals("it")){
				urllenguage = "it_IT";				
			}
			else if (sLanguage.equals("fi")){
				urllenguage = "fi_FI";				
			}
			else if (sLanguage.equals("sr")){//sr_RS_latin //sr_RS //sh
				urllenguage = "sr_RS_latin";				
			}
			else if (sLanguage.equals("sh")){//sr_RS_latin //sr_RS //sh
				urllenguage = "sr_RS";				
			}
			else{
				urllenguage = "en_GB";
			}
			actionResponse.sendRedirect("/"+urllenguage+"/?p_p_id=58&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&saveLastPath=0&_58_struts_action=%2Flogin%2Fcreate_account");
			//actionResponse.sendRedirect("/aac/eauth/cas?language="+sLanguage);
			//https://dev.welive.eu/en_GB/?p_p_id=58&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&saveLastPath=0&_58_struts_action=%2Flogin%2Fcreate_account


		}
	}
}
